const findAll = require("../models/User");
const User = require("../models/User");

class UserController{

    async index(req, res){
        var users = await User.findAll();
        res.json(users);
    }

    async findUser(req, res){

        var id = req.params.id;
        var user = await User.findById(id);
        
        if (user == undefined) {
            res.status(404);
            res.json({});
        } else {
            res.status(200);
            res.json(user);
        }
    }

    async create(req, res){

        var {name, email, password} = req.body;

        if (email == undefined) {
            res.status(400);
            res.json({err:"Error Email"});
            return;
        }

        var emailExists = await User.findEmail(email);

        if (emailExists) {
            res.status(406);
            res.json({err: "E-mail already registered"});
            return;
        }

        await User.new(email, password, name);

        res.status(200);
        res.send("everything OK");
    }
};

module.exports = new UserController();