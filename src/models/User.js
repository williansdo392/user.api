const knex = require("../database/connectionDB");
const bcrypt = require("bcrypt");
const UserController = require("../controllers/UserController");

class User{

    async findAll(){
        try {
            var result = await knex.select("id","name","email","role").table("users");
            return result;
        } catch (err) {
            console.log(err);
            return[];
        }
    }

    async findById(id){
        try {
            var result = await knex.select("id","name","email","role").where({id: id}).table("users");
            return result;
        } catch (err) {
            console.log(err);
            return[];
        }
    }

    async new(name, email, password){
        try{
            var hash = await bcrypt.hash(password, 10);
            await knex.insert({email, password: hash,name,role: 0}).table("users");
        }catch (err) {
            console.log(err);
        }
    }

    async findEmail(email){
        try{
            var result = await knex.select("*").from("user").where({email: email});

            if (result.length > 0) {
                return true;
            }else{
                return false;
            }
        }catch (err) {
            console.log(err);
            return false;
        }
    }
}

module.exports = new User();