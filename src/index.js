const express = require("express")
const app = express();
const bodyParser = require("body-parser")
const helmet = require("helmet")
const cors = require("cors");
const router = require("./routes/routes");

app.use(express());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(helmet());
app.use(cors());

app.use("/", router);

app.listen(8080,() => {
    console.log("Server running!")
});